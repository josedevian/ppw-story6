from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import StatusInput

response = {}

# Create your views here.

def home(request):
    statusList = list(StatusInput.objects.all())
    statusList.reverse()
    response['result'] = statusList
    response['form'] = StatusForm
    return render(request, 'index.html', response)

def add_result(request):
    form = StatusForm(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        StatusInput.objects.create(**data)
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
