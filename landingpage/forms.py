from django import forms

class StatusForm(forms.Form):
    errorMessage = {
        'required': 'Please fill this one before submitting.',
    }


    statusMessage = forms.CharField(label='Status', max_length=300, widget=forms.Textarea(attrs={'placeholder': "What's up?", 'class': 'text-field', id:'content-text'}))
