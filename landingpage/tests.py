from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import home as landingpage
from .models import StatusInput
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class LandingPageUnitTest(TestCase):
    def test_url_landingpage_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_model_can_add_new_status(self):
        statusMessage = StatusInput.objects.create(statusMessage='Halo, saya lagi mencoba-coba')

        counting_all_statusMessage = StatusInput.objects.all().count()
        self.assertEqual(counting_all_statusMessage, 1)

    def test_if_form_is_blank(self):
        form = StatusForm(data={'statusMessage': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['statusMessage'][0],
            'This field is required.',
        )

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/result/', {'statusMessage': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/result/', {'statusMessage': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_self_func(self):
        StatusInput.objects.create(statusMessage='HELLO')
        status = StatusInput.objects.get(id=1)
        self.assertEqual(str(status), status.statusMessage)

    def test_text_max_length(self):
        status = StatusInput.objects.create(statusMessage="TEST TEST TEST TEST TEST TEST")
        self.assertLessEqual(len(str(status)), 300)

class LandingPageFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def test_input(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # Find the form element
        statusMessage = selenium.find_element_by_name('statusMessage')
        submit = selenium.find_element_by_id('submit')
        time.sleep(3)

        # Fill the form with data
        statusMessage.send_keys('Coba Coba')

        # Submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn("Coba Coba", selenium.page_source)

    def test_title(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        time.sleep(3)
        self.assertIn("Status Time!", selenium.title)

    def test_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        header_text = selenium.find_element_by_tag_name('h1').text
        time.sleep(3)
        self.assertIn('Hello, Apa Kabar?', header_text)

    def test_header_with_css_property(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        header_style = selenium.find_element_by_tag_name('h1').value_of_css_property('text-align')
        time.sleep(3)
        self.assertIn('center', header_style)

    def test_body_text_with_css_property(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        body_style = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        time.sleep(3)
        self.assertIn('Muli', body_style)

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()
