
$( function() {
  $( "#accordion" ).accordion({active: false, collapsible: true});
  } );

function ubahTema() {
  var bodyelement = document.getElementById('body')
  if (bodyelement.className == 'body') {
    document.getElementById('body').className = 'body-dark';
    document.getElementById('buttonTheme').className = 'btn btn-light';
    document.getElementByTagName('pre').className = 'darktext';
  }
  else {
    document.getElementById('body').className = 'body';
    document.getElementById('buttonTheme').className = 'btn btn-dark';
    document.getElementByTagName('pre').className = 'lighttext';
  }
}
