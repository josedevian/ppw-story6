from django.apps import AppConfig


class SubspageConfig(AppConfig):
    name = 'subspage'
