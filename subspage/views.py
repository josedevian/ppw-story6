from django.http import JsonResponse
from django.shortcuts import render
from .forms import SubsForm
from .models import SubsData

# Create your views here.

def sub(request):
    return render(request, 'subscribe.html', {'form': SubsForm})

def validateEmail(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = SubsData.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})

def saveToModel(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        SubsData.objects.create(name=name, email=email, password=password)
        return JsonResponse({'is_success': True})

def renderSubscriberList(request):
    if request.method == 'POST':
        data = list(SubsData.objects.all().values())
        return JsonResponse({'data': data})

def deleteSubscriber(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        data = SubsData.objects.filter(email=email, password=password)
        if data.exists():
            data.delete()
            return JsonResponse({'deleted': True})
        else:
            return JsonResponse({'deleted': False})
