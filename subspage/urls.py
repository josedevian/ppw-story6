from django.urls import path
from .views import sub, validateEmail, saveToModel, deleteSubscriber, renderSubscriberList

urlpatterns = [
    path('', sub, name="subscribe"),
    path('validate/', validateEmail, name="validate"),
    path('post/', saveToModel, name="post"),
    path('render/', renderSubscriberList, name="render"),
    path('delete/', deleteSubscriber, name="delete"),

]
